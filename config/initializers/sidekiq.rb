Sidekiq.configure_client do |config|
  config.redis = Routenbuch.config[:redis] || {}
end

Sidekiq.configure_server do |config|
  config.redis = Routenbuch.config[:redis] || {}
end
