require 'simple_navigation_bootstrap/custom_bootstrap'

SimpleNavigation::Configuration.run do |navigation|
  navigation.renderer = SimpleNavigationBootstrap::CustomBootstrap

  navigation.items do |primary|
    primary.item(
      :dashboard,
      {
        icon: 'dashboard',
        text: _('Dashboard')
      }, admin_root_path,
      highlights_on: %r(^/admin/?$)
    )
    primary.item(
      :users,
      {
        icon: 'user',
        text: _('Users')
      }, members_admin_users_path,
      highlights_on: %r(^/admin/users)
    )
    primary.item(
      :monitoring,
      {
        icon: 'thermometer',
        text: _('Monitoring')
      }, admin_monitoring_path,
      highlights_on: %r(^/admin/monitoring)
    )
    primary.item(
      :background_jobs,
      {
        icon: 'gears',
        text: _('Background jobs')
      }, admin_background_jobs_path,
      highlights_on: %r(^/admin/background_jobs)
    )
    primary.item(
      :sys_info,
      {
        icon: 'info-circle',
        text: _('System information')
      }, admin_sys_info_path,
      highlights_on: %r(^/admin/sys_info)
    )
  end
end
