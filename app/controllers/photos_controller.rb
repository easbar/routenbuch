class PhotosController < ApplicationController
  before_action :set_photo, only: [:show, :edit, :update, :destroy]
  before_action :set_base, only: [:index, :new, :create]
  skip_authorization_check :only => [:index]

  def index
    @q = @base_relation.ransack(params[:q])
    @photos = @q.result(distinct: true) \
      .accessible_by(current_ability, :read) \
      .includes(:user, :target)
      .order(**base_order) \
      .page(params[:page])

    respond_to do |format|
      format.html
      format.json do
        render json: PhotoSerializer.new(@photos).serializable_hash.to_json
      end
    end
  end

  def show
    authorize! :read, @photo
  end

  def new
    @photo = Photo.new
    @photo.target = @base_object
    @photo.user = current_user
    authorize! :create, @photo
  end

  def edit
    authorize! :update, @photo
  end

  def create
    @photo = Photo.new(photo_params)
    @photo.target = @base_object
    @photo.user = current_user
    authorize! :create, @photo

    respond_to do |format|
      if @photo.save
        format.html { redirect_to @photo.target.as_base_class, notice: _('Photo was successfully created.') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize! :update, @photo
    respond_to do |format|
      if @photo.update(photo_params)
        format.html { redirect_to @photo, notice: _('Photo was successfully updated.') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    authorize! :destroy, @photo
    @photo.destroy
    respond_to do |format|
      format.html { redirect_to @photo.target.as_base_class, notice: _('Photo was successfully destroyed.') }
    end
  end

  private

  def base_order
    case @base_object
    when Route, GeoRef
      { pinned: :desc, created_at: :desc }
    else
      { created_at: :desc }
    end
  end

  def set_base
    @base_object, @base_relation = \
      if params[:route_id]
        route = Route.find(params[:route_id])
        [ route, route.photos ]
      elsif params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.photos ]
      elsif params[:user_id]
        user = User.find(params[:user_id])
        [ user, user.photos ]
      else
        [ nil, Photo ]
      end
  end

  def set_photo
    @photo = Photo.find(params[:id])
  end

  def photo_params
    params.require(
      :photo
    ).permit(
      :photo,
      :description,
      :pinned,
      :private,
      :cover_photo
    )
  end
end
