class RoutesController < ApplicationController
  before_action :set_route, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => %i[index autocomplete]

  def autocomplete
    @routes = Route \
      .where('name ILIKE (?)', "%#{params[:term]}%") \
      .includes(:geo_ref) \
      .includes(:grade) \
      .accessible_by(current_ability, :read) \
      .limit(10)
    render(
      json: @routes.map do |r|
        {
          id: r.id,
          value: r.name,
          label: "#{r.geo_ref.name} / #{r.name} (#{r.grade.grade})",
        }
      end
    )
  end

  def index
    @q = Route.ransack(params[:q])
    @routes = @q.result \
      .accessible_by(current_ability, :read) \
      .order(created_at: :desc) \
      .page(params[:page])
    respond_to do |format|
      format.html 
      format.json { render json: @routes.to_json( :include => [:geo_ref, :grade] ) }
    end
  end

  def show
    authorize! :read, @route
  end

  def new
    @route = Route.new
    if params[:geo_ref_id].present?
      @route.geo_ref = GeoRef.find(params[:geo_ref_id])
    end
    authorize! :create, @route
  end

  def edit
    authorize! :update, @route
  end

  def create
    @route = Route.new(route_params)
    authorize! :create, @route

    if @route.save
      data = {
        route_name: @route.name,
        route_id: @route.id,
      }
      data.merge!( {
        grade: @route.grade.grade,
        grade_scale: @route.grade.scale,
        grade_category: @route.grade.category,
      } ) if @route.grade.present? 
      data.merge!( {
        geo_ref_name: @route.geo_ref.name,
        geo_ref_id: @route.geo_ref.id,
      } ) if @route.geo_ref.present? 
      data.merge!( {
        first_ascent_person_name: @route.first_ascent_person.name,
        first_ascent_person_id: @route.first_ascent_person.id,
      } ) if @route.first_ascent_person.present? 

      redirect_to @route, notice: _('Route was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @route
    if @route.update(route_params)
      redirect_to @route, notice: _('Route was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @route
    @route.destroy
    redirect_to routes_url, notice: _('Route was successfully destroyed.')
  end

  private

  def set_route
    @route = Route.find(params[:id])
  end

  def route_params
    p = params.require(:route).permit(
      :name,
      :body,
      :grade_id,
      :first_ascent_person_id,
      :first_ascent_year,
      :first_ascent_month,
      :first_ascent_day,
      :geo_ref_id,
      :description,
      :pitches,
      tag_ids: [],
      alternative_names: []
    )
    p[:alternative_names]&.reject!(&:blank?)
    p
  end
end
