class PathsController < ApplicationController
  before_action :set_path, only: %i[show edit update destroy add_geo_ref remove_geo_ref map_items]
  before_action :set_geo_ref, only: %i[new create]
  before_action :set_base, only: [:index]
  skip_authorization_check only: %i[index]

  def index
    unless @base_object.nil?
      raise CanCan::AccessDenied.new("Not authorized!", :read, @base_object) \
        unless can?(:read, @base_object)
    end

    @q = @base_relation.ransack(params[:q])
    @paths = @q.result \
      .accessible_by(current_ability, :read) \
      .includes(:geo_refs) \
      .page(params[:page])
  end

  def show
    authorize! :read, @path
  end

  def map_items
    authorize! :read, @path
    geo_ref = @path.geo_ref

    items = []
    items << geo_ref if can?(:read, geo_ref) && geo_ref.location?
    items += @path.geo_refs.with_location.accessible_by(current_ability, :read)

    render json: GeoRef::MapItemSerializer.new(items).serializable_hash.to_json
  end

  def new
    @path = Path.new
    @path.geo_ref = @geo_ref
    authorize! :create, @path
  end

  def edit
    authorize! :update, @path
  end

  def create
    @path = Path.new(path_params)
    @path.geo_ref = @geo_ref
    authorize! :create, @path
    if @path.save
      redirect_to path_path(@path), notice: _('Path was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @path
    if @path.update(path_params)
      redirect_to path_path(@path), notice: _('Path was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @path
    @path.destroy
    redirect_to paths_url, notice: _('Path was successfully destroyed.')
  end

  def add_geo_ref
    authorize! :update, @path
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :update, @geo_ref
    @path.geo_refs << @geo_ref
    redirect_to path_path(@path), notice: _('Location has been successfully added.')
  end

  def remove_geo_ref
    authorize! :update, @path
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :update, @geo_ref
    @path.geo_refs.delete(@geo_ref)
    redirect_to path_path(@path), notice: _('Location has been successfully removed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.paths ]
      else
        [ nil, Path ]
      end
  end

  def set_path
    @path = Path.find(params[:id])
  end

  def set_geo_ref
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :read, @geo_ref
  end

  def path_params
    params.require(:path).permit(
      :name,
      :type,
      :body,
      :access,
      :geo_ref_id,
      :track
    )
  end
end
