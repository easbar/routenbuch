class AscentsController < ApplicationController
  before_action :set_ascent, only: [:edit, :update, :destroy]
  before_action :set_base, only: [:index]
  skip_authorization_check :only => [:index]

  include RequireFeatures
  require_feature :ascents

  def index
    unless @base_object.nil?
      raise CanCan::AccessDenied.new("Not authorized!", :read, @base_object) \
        unless can?(:read, @base_object) || can?(:read_public, @base_object)
    end

    @q = @base_relation.ransack(params[:q])
    @ascents = @q.result \
      .includes(:style, :user, route: [:geo_ref, :grade]) \
      .accessible_by(current_ability, :read) \
      .distinct(false) \
      .order(when: :desc) \
      .page(params[:page])
  end

  def new
    @ascent = Ascent.new
    @ascent.user = current_user
    @ascent.attempts = 1
    if params[:route_id].present?
      @ascent.route = Route.find(params[:route_id])
    end
    @ascent.when = current_user&.last_ascent&.when
    authorize! :create, @ascent
  end

  def edit
    authorize! :update, @ascent
  end

  def create
    @ascent = Ascent.new(ascent_params)
    @ascent.user = current_user
    authorize! :create, @ascent

    if @ascent.save
      if @ascent.style.redpoint
        ticklists = Ticklist.where(
          :user => @ascent.user, :route => @ascent.route,
        )
        if ticklists.one?
          ticklists.first.delete
          alert = _('Removed route from ticklist!')
        end
      end

      redirect_to geo_ref_path(@ascent.route.geo_ref),
        notice: _('Ascent was successfully created.'), alert: alert
    else
      render :new
    end
  end

  def update
    authorize! :update, @ascent
    if @ascent.update(ascent_params)
      redirect_to user_ascents_path(current_user), notice: _('Ascent was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @ascent
    @ascent.destroy
    redirect_to user_ascents_path(current_user), notice: _('Ascent was successfully removed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:user_id]
        user = User.find(params[:user_id])
        [ user, user.ascents ]
      elsif params[:route_id]
        route = Route.find(params[:route_id])
        [ route, route.ascents ]
      elsif params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.ascents ]
      else
        [ nil, Ascent ]
      end
  end

  def set_ascent
    @ascent = Ascent.find(params[:id])
  end

  def ascent_params
    params.require(:ascent).permit(:user_id, :route_id, :comment, :when, :style_id, :attempts)
  end
end
