class SearchController < ApplicationController
  skip_authorization_check only: [:index]

  def index
    @q = params[:q]
    {
      geo_refs: GeoRef,
      routes: Route,
      first_ascent_people: FirstAscentPerson
    }.each do |name, model|
      relation = if @q.present?
                   model.search_full_text(@q) \
                     .accessible_by(current_ability, :read) \
                     .limit(10)
                 else
                   nil
                 end
      instance_variable_set("@#{name}", relation)
    end
    if @first_ascent_people&.any?
      @fa_counts = Route.where(first_ascent_person_id: @first_ascent_people.map(&:id)) \
        .accessible_by(current_ability, :read) \
        .group(:first_ascent_person_id) \
        .count
    end
  end
end
