class PermissionsController < ApplicationController
  before_action :set_geo_ref
  before_action :set_permission, only: %i[edit update destroy]
  skip_authorization_check :only => %i[index]

  def index
    @permissions = @geo_ref \
      .permissions \
      .accessible_by(current_ability, :read)
    @inherited_permissions = @geo_ref \
      .inherited_permissions \
      .accessible_by(current_ability, :read)
    @new_permission ||= Permission.new(geo_ref: @geo_ref)
  end

  def create
    @new_permission = Permission.new(permission_params)
    authorize! :create, @new_permission
    if @geo_ref.permissions << @new_permission
      redirect_to geo_ref_permissions_path(@geo_ref), notice: _('Permission was successfully created.')
    else
      index
      render :index
    end
  end

  def edit
    authorize! :update, @permission
  end

  def update
    authorize! :update, @permission
    if @permission.update(permission_params)
      redirect_to geo_ref_permissions_path(@geo_ref), notice: _('Permission was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @permission
    @permission.destroy
    redirect_to geo_ref_permissions_path(@geo_ref), notice: _('Permission was successfully removed.')
  end

  private

  def set_geo_ref
    @geo_ref = GeoRef.find(params.require(:geo_ref_id))
    authorize! :read, @geo_ref
  end

  def permission_params
    params.require(:permission).permit(
      :user_id,
      :level
    )
  end

  def set_permission
    @permission = Permission.find(params[:id])
  end
end
