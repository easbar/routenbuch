class ProfileController < ApplicationController
  before_action :user

  def show
    authorize! :read, @user
  end

  def update
    authorize! :update_settings, @user
    if @user.update(profile_params)
      redirect_to profile_path, notice: _('Profile was successfully updated.')
    else
      render :show
    end
  end

  def password
    authorize! :read, @user
  end
  
  def update_password
    authorize! :update_settings, @user
    if @user.update_with_password(profile_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to profile_password_path, notice: _('Password was successfully updated.')
    else
      render "password"
    end
  end

  def tab_nav
    @tab_nav ||= proc do |primary|
      primary.item :profile, _('Settings'), profile_path
      primary.item :password, _('Password'), profile_password_path
      primary.dom_class = 'nav nav-tabs'
    end
  end
  helper_method :tab_nav

  private

  def user
    @user = current_user
  end

  def profile_params
    params.require(:user).permit(
      :avatar,
      :name,
      :bio,
      :locale,
      :password,
      :password_confirmation,
      :current_password,
      :access,
      :ascents_access,
      :ticklists_access
    )
  end
end
