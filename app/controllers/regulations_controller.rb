class RegulationsController < ApplicationController
  before_action :set_regulation, only: [:edit, :update, :destroy, :show]
  before_action :set_base, only: [:index, :new, :create]
  skip_authorization_check :only => [:index]

  def index
    authorize! :read, @base_object unless @base_object.nil?

    @q = @base_relation.ransack(params[:q])
    @regulations = @q.result \
      .includes(:geo_ref) \
      .accessible_by(current_ability, :read) \
      .order(name: :asc) \
      .page(params[:page])
  end

  def new
    @regulation = Regulation.new
    authorize! :create, @regulation
  end

  def edit
    authorize! :update, @regulation
  end

  def show
    authorize! :read, @regulation
    @regulated_geo_refs = @regulation.regulated_geo_refs \
      .accessible_by(current_ability, :read) \
      .includes(
        geo_ref: [
          :zone,
          { secondary_zones: :zone }
        ]
      ).order('geo_refs.name')
  end

  def create
    @regulation = Regulation.new(regulation_params)
    @regulation.geo_ref = @base_object
    authorize! :create, @regulation

    if @regulation.save
      redirect_to regulation_path(@regulation), notice: _('Regulation was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @regulation
    if @regulation.update(regulation_params)
      redirect_to regulation_path(@regulation), notice: _('Regulation was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @regulation
    @regulation.destroy
    redirect_to regulations_path, notice: _('Regulation was successfully removed.')
  end

  private

  def set_base
    @base_object, @base_relation = \
      if params[:geo_ref_id]
        geo_ref = GeoRef.find(params[:geo_ref_id])
        [ geo_ref, geo_ref.regulations ]
      else
        [ nil, Regulation ]
      end
  end
  
  def set_regulation
    @regulation = Regulation.find(params[:id])
  end

  def regulation_params
    params.require(:regulation).permit(
      :name,
      :abbr,
      :body
    )
  end
end
