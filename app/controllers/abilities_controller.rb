class AbilitiesController < ApplicationController
  before_action :set_table
  skip_authorization_check :only => %i[show]

  def show
  end

  private

  def set_table
    @table = case params[:id]
             when 'geo_ref_roles'
               AbilityPresenters::TablePresenter.new do |p|
                 p.add_users_with_roles
                 p.add_geo_ref_actions
               end
             when 'geo_ref_contributor'
               AbilityPresenters::TablePresenter.new do |p|
                 p.add_contributor_users
                 p.add_geo_ref_actions
               end
             when 'comments'
               AbilityPresenters::TablePresenter.new do |p|
                 p.add_users_with_roles
                 p.add_user_owner
                 p.add_contributor_users
                 p.add_comments_actions
               end
             else
               raise ActiveRecord::RecordNotFound
             end
  end
end
