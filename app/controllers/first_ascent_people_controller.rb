class FirstAscentPeopleController < ApplicationController
  before_action :set_first_ascent_person, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => [:index, :autocomplete]

  def autocomplete
    @first_ascent_people = FirstAscentPerson \
      .where('first_name ILIKE (?) OR last_name ILIKE(?)', "%#{params[:term]}%", "%#{params[:term]}%") \
      .accessible_by(current_ability, :read) \
      .limit(5)
    render(
      json: @first_ascent_people.map do |f|
        {
          id: f.id,
          value: f.name,
          label: f.name
        }
      end
    )
  end

  def index
    @q = FirstAscentPerson.ransack(params[:q])
    @first_ascent_people = @q.result \
      .accessible_by(current_ability, :read) \
      .page(params[:page])
    # eager load counts
    @fa_counts = Route.where(first_ascent_person_id: @first_ascent_people.map(&:id)) \
      .accessible_by(current_ability, :read) \
      .group(:first_ascent_person_id) \
      .count
  end

  def show
    authorize! :read, @first_ascent_person
    @routes = @first_ascent_person \
      .route \
      .joins(:grade) \
      .accessible_by(current_ability, :read) \
      .order('grades.difficulty DESC')
  end

  def new
    @first_ascent_person = FirstAscentPerson.new
    authorize! :create, @first_ascent_person
  end

  def edit
    authorize! :update, @first_ascent_person
  end

  def create
    @first_ascent_person = FirstAscentPerson.new(first_ascent_person_params)
    authorize! :create, @first_ascent_person

    respond_to do |format|
      if @first_ascent_person.save
        format.html { redirect_to @first_ascent_person, notice: _('First ascent person was successfully created.') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize! :update, @first_ascent_person
    respond_to do |format|
      if @first_ascent_person.update(first_ascent_person_params)
        format.html { redirect_to @first_ascent_person, notice: _('First ascent person was successfully updated.') }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    authorize! :destroy, @first_ascent_person
    @first_ascent_person.destroy
    respond_to do |format|
      format.html { redirect_to first_ascent_people_url, notice: _('First ascent person was successfully destroyed.') }
    end
  end

  private

  def set_first_ascent_person
    @first_ascent_person = FirstAscentPerson.find(params[:id])
  end

  def first_ascent_person_params
    p = params \
      .require(:first_ascent_person) \
      .permit(
        :first_name,
        :last_name,
        :dead,
        alternative_names: []
      )
    p[:alternative_names]&.reject!(&:blank?)
    p
  end
end
