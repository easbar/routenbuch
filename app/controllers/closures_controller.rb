class ClosuresController < ApplicationController
  before_action :set_closure, only: %i[show edit update destroy add_geo_ref remove_geo_ref]
  skip_authorization_check only: %i[index browse]

  def index
    @q = Closure.ransack(params[:q])
    @closures = @q.result.accessible_by(current_ability, :read).includes(:geo_refs).page(params[:page])
  end

  def show
    authorize! :read, @closure
  end

  def new
    @closure = Closure.new
    authorize! :create, @closure
  end

  def edit
    authorize! :update, @closure
  end

  def create
    @closure = Closure.new(closure_params)
    authorize! :create, @closure
    if @closure.save
      redirect_to @closure, notice: _('Closure was successfully created.')
    else
      render :new
    end
  end

  def update
    authorize! :update, @closure
    if @closure.update(closure_params)
      redirect_to @closure, notice: _('Closure was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @closure
    @closure.destroy
    redirect_to closures_url, notice: _('Closure was successfully destroyed.')
  end

  def add_geo_ref
    authorize! :update, @closure
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :update, @geo_ref
    @closure.geo_refs << @geo_ref
    redirect_to @closure, notice: _('Location has been successfully added.')
  end

  def remove_geo_ref
    authorize! :update, @closure
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :update, @geo_ref
    @closure.geo_refs.delete(@geo_ref)
    redirect_to @closure, notice: _('Location has been successfully removed.')
  end

  private

  def set_closure
    @closure = Closure.find(params[:id])
  end

  def closure_params
    params.require(:closure).permit(
      :description,
      :regular_start_month,
      :regular_start_day_of_month,
      :regular_end_month,
      :regular_end_day_of_month,
      :kind
    )
  end
end
