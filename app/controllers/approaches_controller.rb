class ApproachesController < ApplicationController
  before_action :set_geo_ref, only: %i[index]
  skip_authorization_check only: %i[index]

  def index
    @q = @geo_ref.approaches.ransack(params[:q])
    @approaches = @q.result \
      .accessible_by(current_ability, :read) \
      .includes(:geo_refs) \
      .page(params[:page])
  end

  private

  def set_geo_ref
    @geo_ref = GeoRef.find(params[:geo_ref_id])
    authorize! :read, @geo_ref
  end
end
