class Admin::UsersController < Admin::ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_authorization_check :only => [:index, :autocomplete]

  def autocomplete
    @users = User \
      .where('name ILIKE (?)', "%#{params[:term]}%") \
      .accessible_by(current_ability, :admin) \
      .limit(5)
    render(
      json: @users.map do |f|
        {
          id: f.id,
          value: f.name,
          label: "#{f.name} <#{f.email}>"
        }
      end
    )
  end

  def index
    @search_url ||= admin_users_path
    @collection ||= User
    @q = @collection.ransack(params[:q])
    @users = @q.result \
      .accessible_by(current_ability, :admin) \
      .order(created_at: :desc) \
      .page(params[:page])
    @users_count = User.accessible_by(current_ability, :admin).count \
    @members_count = User.accessible_by(current_ability, :admin).where(role: 'member').count
    @contributors_count = User.accessible_by(current_ability, :admin).where(role: 'contributor').count
    @admins_count = User.accessible_by(current_ability, :admin).where(role: 'admin').count
  end

  def members
    @search_url = members_admin_users_path
    @collection = User.where(role: 'member')
    index
    render :index
  end

  def contributors
    @search_url = contributors_admin_users_path
    @collection = User.where(role: 'contributor')
    index
    render :index
  end

  def admins
    @search_url = admins_admin_users_path
    @collection = User.where(role: 'admin')
    index
    render :index
  end

  def show
    authorize! :admin, @user
    @permissions = @user.permissions \
      .accessible_by(current_ability, :read)
  end

  def new
    @user = User.new
    authorize! :admin, @user
  end

  def edit
    authorize! :admin, @user
  end

  def create
    @user = User.new(user_params)
    authorize! :admin, @user

    respond_to do |format|
      if @user.save
        format.html { redirect_to admin_user_path(@user), notice: _('User was successfully created.') }
      else
        format.html { render :new }
      end
    end
  end

  def update
    authorize! :admin, @user
    respond_to do |format|
      if @user.update_without_password(user_params)
        format.html { redirect_to admin_user_path(@user), notice: _('User was successfully updated.') }
      else
        logger.info @user.errors
        format.html { render :edit }
      end
    end
  end

  def destroy
    authorize! :admin, @user
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url, notice: _('User was successfully removed.') }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params \
      .require(:user) \
      .permit(
        :name,
        :email,
        :role,
        :password,
        :password_confirmation
      )
  end
end
