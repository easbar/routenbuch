class AccessLevelValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    levels = %w[confidential private internal]
    levels << 'public' if Routenbuch.public?
    unless levels.include?(value)
      record.errors[attribute] << (options[:message] || "is not a valid access level")
    end
  end
end
