class GeoRefUpdateStatsJob < ApplicationJob
  queue_as :update_stats

  def perform(geo_ref_id)
    geo_ref = GeoRef.find(geo_ref_id)
    return unless geo_ref

    ([geo_ref] + geo_ref.parents).each do |g|
      g.update_stats
      g.save!
    end
  end
end
