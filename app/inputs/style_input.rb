class StyleInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    initial_id = 0
    initial_value = ''
    name = options[:name]

    raise "must be a belong_to association" unless association_macro == :belongs_to

    name ||= "#{@builder.object_name}[#{attribute_name}_id]"
    associated_object = @builder.object.send(attribute_name)

    if associated_object
      initial_id = associated_object.id
      initial_value = label_for_object(associated_object)
    end

    template.content_tag(
      'style-input',
      '',
      name: name,
      ':initial-id': initial_id || 'null',
      'initial-value': initial_value,
      url: options[:autocomplete_url],
      ':belay-styles': Style.belay_styles.to_json,
      ':sportive-styles': Style.sportive_styles.to_json,
    ).html_safe
  end

  private

  def association_macro
    reflection = @builder.object.class.reflect_on_association(attribute_name)
    return unless reflection

    reflection.macro
  end

  def label_for_object(obj)
    if obj.respond_to?(:name)
      obj.name
    else
      obj.id
    end
  end
end
