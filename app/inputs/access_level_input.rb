class AccessLevelInput < SimpleForm::Inputs::Base
  def input(wrapper_options)
    levels = %w[confidential private internal]
    levels << 'public' if Routenbuch.public?
    @builder.select(attribute_name, levels, {}, input_html_options)
  end

  def input_html_options
    super.merge({class: 'form-control'})
  end
end
