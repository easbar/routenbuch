module UserPresenters
  # presenter class to generate user statistics data
  class StatsPresenter
    attr_reader :user

    def initialize(user)
      @user = user
    end

    def routes_redpoint
      Route.distinct.joins(:ascents => :style)
        .where('ascents.user_id' => user.id, 'styles.redpoint' => 't')
    end

    def route_grade_category_count
      Route.where(:id => routes_redpoint).joins(:grade)
        .group('grades.category')
        .order('MAX(grades.difficulty)')
        .count
    end

    def route_grade_difficulty_count
      Route.where(:id => routes_redpoint).joins(:grade)
        .group('grades.difficulty')
        .order('MAX(grades.difficulty)')
        .pluck("string_agg(distinct grades.grade, ',')", 'COUNT(routes.id)')
    end

    def ascents_by_style
      user.ascents \
        .joins(:style) \
        .group('styles.name') \
        .pluck('styles.name', 'COUNT(ascents.id)')
    end

    def ascents_by_style_and_difficulty
      result = user.ascents \
        .joins(:style) \
        .joins(route: :grade)
        .group('grades.difficulty', 'styles.name') \
        .order('MAX(grades.difficulty)')
        .pluck("string_agg(distinct grades.grade, ',')", 'styles.name', 'COUNT(routes.id)')

      grades = result.map { |i| i[0] }.uniq
      styles = result.map { |i| i[1] }.uniq

      # build a zerod out table
      series = styles.map do |style|
        [
          style,
          grades.map do |grade|
            [ grade, 0 ]
          end.to_h
        ]
      end.to_h

      # fill in values where present
      result.each do |row|
        series[row[1]][row[0]] = row[2]
      end

      # reformat for chartkick series data
      series.map do |name, data|
        # convert data to array as object are not sorted in js
        { name: name, data: data.to_a }
      end
    end

    def stats
      {
        ascents_by_style: ascents_by_style,
        ascents_by_style_and_difficulty: ascents_by_style_and_difficulty,
        route_grade_category_count: route_grade_category_count,
        route_grade_difficulty_count: route_grade_difficulty_count,
        routes_count: routes_redpoint.count,
      }
    end
  end
end
