class Route < ApplicationRecord
  belongs_to :geo_ref
  delegate :effective_permissions, to: :geo_ref
  belongs_to :grade, optional: true
  belongs_to :first_ascent_person, optional: true
  has_many :ascents, dependent: :destroy
  has_many :ticklists, dependent: :destroy
  has_many :photos, as: :target, dependent: :destroy
  has_many :comments, as: :target, dependent: :destroy

  validates_presence_of :name

  before_validation do
    # dont set on single pitch routes
    self.pitches = nil if self.pitches == 1
  end

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if geo_ref.nil?
    return if geo_ref.is_a? Crag

    errors.add(:geo_ref, _('Routes can only be attached to a crag. (not to a %{type})') % {type: geo_ref.class.name})
  end

  validates(
    :first_ascent_year,
    numericality: {
      only_integer: true,
      greater_than: 1900,
      less_than: 2100
    },
    allow_nil: true
  )
  validates(
    :first_ascent_month,
    numericality: {
      only_integer: true,
      greater_than: 0,
      less_than: 13
    },
    allow_nil: true
  )
  validates(
    :first_ascent_day,
    numericality: {
      only_integer: true,
      greater_than: 0,
      less_than: 32
    },
    allow_nil: true
  )

  validate :validate_first_ascent_date
  def validate_first_ascent_date
    errors.add(:first_ascent_date, 'Year is missing.') \
      if first_ascent_month.present? && first_ascent_year.blank?

    errors.add(:first_ascent_date, 'Month is missing') \
      if first_ascent_day.present? && first_ascent_month.blank?

    if first_ascent_year.present? \
        && first_ascent_month.present? \
        && first_ascent_day.present?
      begin
        Date.new(first_ascent_year, first_ascent_month, first_ascent_day)
      rescue ArgumentError
        errors.add(:first_ascent_date, 'Invalid date') \
      end
    end
  end
  after_validation :copy_first_ascent_errors
  def copy_first_ascent_errors
    %i[
      first_ascent_year
      first_ascent_month
      first_ascent_day
    ].each do |field|
      errors[field].each do |error|
        errors.add(:first_ascent_date, error)
      end
    end
  end

  def first_ascent_date
    [first_ascent_year, first_ascent_month, first_ascent_day]
  end

  include Taggable

  def self.ransackable_attributes(auth_object = nil)
    %w[name description pitches]
  end

  def self.ransackable_associations(auth_object = nil)
    %w[grade first_ascent_person geo_ref]
  end

  def self.ransackable_scopes(auth_object = nil)
    %i[search_full_text]
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name:   'A',
      description: 'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  def user_ascents(user)
    ascents.where(:user => user)
  end

  def user_has_redpoint(user)
    user_ascents(user).joins(:style).where(styles: { redpoint: true }).any?
  end

  def access
    geo_ref.access
  end

  after_commit do
    trigger_geo_ref_update_stats
  end

  attr_accessor :skip_geo_ref_update_stats
  def trigger_geo_ref_update_stats
    return if skip_geo_ref_update_stats
    return if saved_changes.except('priority', 'updated_at').empty?

    geo_ref.update_self_and_parents_stats
  end

  before_create :set_initial_priority
  def set_initial_priority
    max = geo_ref.routes.maximum(:priority)
    self.priority = max.nil? ? 0 : (max+1)
  end

  def self.icon
    'book'
  end
end
