class Closure < ApplicationRecord
  has_many :season_closures,
    dependent: :delete_all

  has_many :geo_refs,
    dependent: :nullify

  validates :description, presence: true
  validates :regular_start_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_start_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :regular_end_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 12
    }
  validates :regular_end_day_of_month,
    numericality: {
      allow_nil: true,
      greater_than_or_equal_to: 1,
      less_than_or_equal_to: 31
    }
  validates :kind,
    inclusion: {
      in: %w[flexible temporary decree]
    }

  def self.icon
    'lock'
  end

  def ransackable_attributes(auth_object = nil)
    %w[description kind]
  end

  def ransackable_associations(auth_object = nil)
    %w[geo_refs]
  end

  def start_year
    @start_year ||= if multi_year? && !after_end?
                      Time.new.year - 1
                    else
                      Time.new.year
                    end
  end

  def end_year
    @end_year ||= if multi_year? && after_end?
                    Time.new.year + 1
                  else
                    Time.new.year
                  end
  end

  def after_end?
    build_date(Time.new.year, regular_end_month, regular_end_day_of_month) < Date.today
  end

  def multi_year?
    return false if regular_start_month.nil? \
      || regular_end_month.nil? \
      || regular_start_day_of_month.nil? \
      || regular_end_day_of_month.nil?

    build_date(0, regular_start_month, regular_start_day_of_month) > \
      build_date(0, regular_end_month, regular_end_day_of_month)
  end

  def current_season_closure
    @current_season_closure ||= season_closures.where(year: start_year).first
  end

  def start_month
    @start_month ||= current_season_closure&.start_month || regular_start_month
  end

  def start_day_of_month
    @start_day_of_month ||= current_season_closure&.start_day_of_month || regular_start_day_of_month
  end

  def end_month
    @end_month ||= current_season_closure&.end_month || regular_end_month
  end

  def end_day_of_month
    @end_day_of_month ||= current_season_closure&.end_day_of_month || regular_end_day_of_month
  end

  def start_at
    build_date(start_year, start_month, start_day_of_month)
  end

  def regular_start_at
    build_date(start_year, regular_start_month, regular_start_day_of_month)
  end

  def end_at
    build_date(end_year, end_month, end_day_of_month)
  end

  def regular_end_at
    build_date(end_year, regular_end_month, regular_end_day_of_month)
  end

  def active?
    return false if start_at.nil? || end_at.nil?

    today = Date.today
    today >= start_at && today <= end_at
  end

  protected

  def build_date(year, month, day)
    return if month.nil? || day.nil?

    # use last day of month if day exceeds month days
    last_day_of_month = Date.new(year, month, 1).next_month.prev_day.mday
    Date.new(year, month, day > last_day_of_month ? last_day_of_month : day)
  end
end
