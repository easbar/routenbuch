class Style < ApplicationRecord
  class << self
    BELAY_STYLES = %w[
      lead
      second
      toprope
      boulder
      alternate_lead
      solo
      freesolo
      deepwatersolo
    ].freeze
    SPORTIVE_STYLES = %w[
      free
      flash
      onsight
      tainted
      unfished
      aid
    ].freeze

    def sportive_styles
      SPORTIVE_STYLES
    end

    def belay_styles
      BELAY_STYLES
    end
  end

  has_many :ascents

  validates :name, presence: true, uniqueness: true
  validates(
    :belay_style,
    inclusion: {
      in: Style.belay_styles,
      message: "%{value} is not a valid belay style"
    },
    presence: true
  )
  validates(
    :sportive_style,
    inclusion: {
      in: Style.sportive_styles,
      message: "%{value} is not a valid sportive style"
    },
    presence: true
  )

  def ransackable_attributes(auth_object = nil)
    %w[name redpoint belay_style sportive_style]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
