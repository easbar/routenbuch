class Regulation < ApplicationRecord
  class << self
    def valid_geo_ref_types
      [Country, Region].freeze
    end

    def valid_geo_ref_type?(klass)
      valid_geo_ref_types.include? klass
    end

    def icon
      'file-text-o'
    end
  end

  belongs_to :geo_ref
  has_many :regulated_geo_refs, dependent: :destroy

  validates :name, presence: true

  validate :validate_geo_ref_type
  def validate_geo_ref_type
    return if self.class.valid_geo_ref_type? geo_ref.class

    errors.add(:geo_ref, _('must be a country or region'))
  end

  def self.ransackable_attributes(_auth_object = nil)
    %w[name abbr body]
  end

  def self.ransackable_associations(_auth_object = nil)
    []
  end

  def self.ransackable_scopes(auth_object = nil)
    %i[search_full_text]
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name:   'A',
      abbr:   'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )
end
