class Crag < GeoRef
  class << self
    def valid_parent_classes
      [Region]
    end
  end

  def supports_orientation?
    true
  end

  def supports_height?
    true
  end

  def supports_zone?
    true
  end

  def supports_approaches?
    true
  end
end
