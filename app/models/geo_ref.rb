class GeoRef < ApplicationRecord
  class << self
    def geo_ref_types
      %w[Country Region Crag Sector Parking].freeze
    end

    def icon
      'map-marker'
    end

    def valid_parent_classes
      []
    end

    def valid_parent_class?(klass)
      valid_parent_classes.include?(klass)
    end
  end

  has_many :childs, class_name: "GeoRef", foreign_key: "parent_id", dependent: :nullify
  belongs_to :parent, class_name: "GeoRef", optional: true
  belongs_to :zone, optional: true
  has_many :secondary_zones, dependent: :destroy
  has_many :regulations, dependent: :destroy
  has_many :regulated_geo_refs, dependent: :destroy
  belongs_to :closure, optional: true
  has_many :routes, dependent: :destroy
  has_many :photos, as: :target, dependent: :destroy
  has_many :topos, as: :target, dependent: :destroy
  has_many :comments, as: :target, dependent: :destroy
  has_many :paths, dependent: :destroy
  has_and_belongs_to_many :approaches, class_name: 'Path'

  validates_presence_of :name

  validates(
    :orientation,
    numericality: {
      allow_nil: true,
      less_than: 361
    },
    if: :supports_orientation?
  )
  validates :orientation, absence: true, unless: :supports_orientation?

  validates(
    :height,
    numericality: {
      allow_nil: true,
      less_than: 5000
    },
    if: :supports_height?
  )
  validates :height, absence: true, unless: :supports_height?

  validates :zone, absence: true, unless: :supports_zone?
  validates :secondary_zones, absence: true, unless: :supports_zone?

  validates :access, presence: true, access_level: true

  validates(
    :type,
    inclusion: {
      in: GeoRef.geo_ref_types,
      message: '%{value} is not a valid GeoRef type!'
    }
  )

  validate :validate_parent_type
  def validate_parent_type
    return if parent.nil? && is_a?(Country) 
    
    if parent.nil?
      errors.add(:parent, _('cannot be empty for type %{type}') % { type: self.class.name } )
      return
    end

    unless self.class.valid_parent_class?(parent.class)
      errors.add(
        :parent,
        _('must be of one of the following type: %{types}') % {
          types: self.class.valid_parent_classes.join(', ')
        }
      )
    end
  end
  attr_accessor :skip_parent_changed_validation
  validate :validate_parent_changed, unless: :skip_parent_changed_validation
  def validate_parent_changed
    if parent_id_changed? && self.persisted?
      errors.add(:parent, "cannot be changed")
    end
  end

  include PermissionAssignable
  include Taggable

  scope :with_location, -> { where.not(lat: nil).where.not(lng: nil) }

  def ransackable_attributes(auth_object = nil)
    %w[name type description height orientation]
  end

  def ransackable_associations(auth_object = nil)
    %w[zone routes tags]
  end

  def self.ransackable_scopes(auth_object = nil)
    %i[search_full_text]
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      name:   'A',
      description: 'B',
      body: 'C'
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )

  def supports_orientation?
    false
  end

  def supports_height?
    false
  end

  def supports_zone?
    false
  end

  def supports_ascents?
    Routenbuch.features.ascents?
  end

  def supports_ticklists?
    Routenbuch.features.ticklists?
  end

  def supports_paths?
    false
  end

  def supports_approaches?
    false
  end

  def supports_regulations?
    Regulation.valid_geo_ref_type? self.class
  end

  def update_stats
    self.stats = {
      route_grade_stats: self.route_grade_stats,
      descendent_routes_count: self.descendent_routes.count,
      descendent_crags_count: self.descendent_crags.count,
      descendents_count: self.descendents.count,
      childs_count: self.childs.count,
    }
  end

  def route_grade_stats
    descendent_routes.joins(:grade)
      .group('grades.category')
      .order('MAX(grades.difficulty)')
      .count
  end

  def location?
    self.lat.present? and self.lng.present?
  end
  alias :latlng? :location?

  def location
    return unless latlng?

    Geokit::LatLng.new(lat,lng)
  end

  # search parents for closest location
  def next_location
    cur = self
    loop do
      break if cur.nil?
      return cur.location if cur.location.present?

      cur = cur.parent
    end

    return
  end

  def parents
    parents = []
    cur = self.parent
    while ! cur.nil? do
      parents += [ cur ]
      cur = cur.parent
    end
    parents
  end

  def descendents
    self_and_descendents - [self]
  end

  def self_and_descendents
    GeoRef.tree_for(self)
  end

  def ascents
    Ascent \
      .joins(:route) \
      .where('routes.geo_ref_id': self_and_descendents)
  end

  def ticklists
    Ticklist \
      .joins(:route) \
      .where('routes.geo_ref_id': self_and_descendents)
  end

  def descendent_routes
    subtree = GeoRef.tree_sql_for(self)
    Route.where("geo_ref_id IN (#{subtree})")
  end

  def descendent_crags
    self_and_descendents
      .where(type: 'Crag') - [self]
  end

  def self.tree_for(instance)
    where("#{table_name}.id IN (#{tree_sql_for(instance)})").order("#{table_name}.id")
  end

  def self.tree_sql_for(instance)
    <<-SQL
      WITH RECURSIVE search_tree(id, path) AS (
          SELECT id, ARRAY[id]
          FROM #{table_name}
          WHERE id = #{instance.id}
        UNION ALL
          SELECT #{table_name}.id, path || #{table_name}.id
          FROM search_tree
          JOIN #{table_name} ON #{table_name}.parent_id = search_tree.id
          WHERE NOT #{table_name}.id = ANY(path)
      )
      SELECT id FROM search_tree ORDER BY path
    SQL
  end

  # geokit
  acts_as_mappable :default_units => :kms

  def update_self_and_parents_stats
    GeoRefUpdateStatsJob.perform_later(id)
  end
end
