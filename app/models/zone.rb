class Zone < ApplicationRecord
  has_many :geo_refs
  has_many :secondary_zones

  validates :name, presence: true
  validates :icon_path, presence: true

  def ransackable_attributes(auth_object = nil)
    %w[name organization]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
