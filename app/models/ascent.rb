class Ascent < ApplicationRecord
  belongs_to :user
  belongs_to :route
  belongs_to :style

  validates :when, presence: true
  validates :attempts, presence: true

  after_commit do
    user.after_ascent_commit(self)
  end

  def self.icon
    'address-book-o'
  end

  def ransackable_attributes(auth_object = nil)
    %w[when attempts]
  end

  def ransackable_associations(auth_object = nil)
    %w[route style]
  end
end
