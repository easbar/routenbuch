class Photo < ApplicationRecord
  validates_presence_of :photo, :description, :target
  belongs_to :target, polymorphic: true

  belongs_to :user

  include TargetAccessFilterable

  has_one_attached :photo
  validates(
    :photo,
    content_type: Routenbuch.supported_image_types,
    size: { less_than: Routenbuch.max_upload_size }
  )

  scope(
    :cover_photos,
    lambda do
      where(cover_photo: true).order(Arel.sql('RANDOM()'))
    end
  )

  scope(
    :ids_for_geo_refs_by_groups,
    lambda do |group_ids|
      select(:id) \
        .where(target_type: 'GeoRef') \
        .joins('JOIN geo_refs ON geo_refs.id = target_id') \
        .joins('JOIN geo_refs_groups ON geo_refs_groups.geo_ref_id = geo_refs.id') \
        .where('geo_refs_groups.group_id' => group_ids).distinct
    end
  )

  scope(
    :ids_for_routes_by_groups,
    lambda do |group_ids|
      select(:id) \
        .where(target_type: 'Route') \
        .joins('JOIN routes ON routes.id = target_id') \
        .joins('JOIN geo_refs ON routes.geo_ref_id = geo_refs.id') \
        .joins('JOIN geo_refs_groups ON geo_refs_groups.geo_ref_id = geo_refs.id') \
        .where('geo_refs_groups.group_id' => group_ids).distinct
    end
  )

  scope(
    :all_of_groups,
    lambda do |group_ids|
      union_sql = [
        ids_for_geo_refs_by_groups(group_ids),
        ids_for_routes_by_groups(group_ids),
      ].map do |relation|
        relation.to_sql
      end.join(' UNION ')

      where("photos.id IN (#{union_sql})")
    end
  )

  def self.icon
    'photo'
  end

  def ransackable_attributes(auth_object = nil)
    %w[description]
  end

  def ransackable_associations(auth_object = nil)
    %w[]
  end
end
