class FirstAscentPerson < ApplicationRecord
  has_many :route, dependent: :nullify

  validates :last_name, presence: true

  def self.ransackable_attributes(auth_object = nil)
    %w[first_name last_name]
  end

  def self.ransackable_associations(auth_object = nil)
    %w[]
  end

  def self.ransackable_scopes(auth_object = nil)
    %i[search_full_text]
  end

  def self.icon
    'flash'
  end

  def dead?
    dead
  end

  def name
    unless first_name.blank? && last_name.blank?
      "#{first_name} #{last_name} #{dead? ? '✝' : ''}"
    else
      _('no name')
    end
  end

  include PgSearch::Model
  pg_search_scope(
    :search_full_text,
    against: {
      first_name:   'A',
      last_name:   'A',
    },
    using: {
      tsearch: {
        prefix: true,
        dictionary: 'simple'
      }
    }
  )
end
