module PermissionAssignable
  extend ActiveSupport::Concern

  included do
    has_many(
      :permissions,
      dependent: :destroy,
      after_add: :assign_effective_permission
    )
    has_and_belongs_to_many(
      :effective_permissions,
      class_name: 'Permission',
      join_table: :effective_permissions
    )
  end

  def inherited_permissions
    effective_permissions.where.not(geo_ref: self)
  end

  # clears and re-assigns all effective permissions
  # recursivly below this object
  def rebuild_effective_permissions
    transaction do
      clear_effective_permissions
      assign_parent_effective_permissions
      assign_effective_permissions
    end
  end

  def copy_parent_effective_permissions
    return unless parent.present?

    self.effective_permissions = parent.effective_permissions
  end

  # assigns parent effective permissions to itself
  # and all child objects
  def assign_parent_effective_permissions
    return unless parent.present?
    return unless parent.effective_permissions.any?

    EffectivePermission.insert_all(
      self_and_descendents.map do |g|
        parent.effective_permissions.map do |p|
          {
            geo_ref_id: g.id,
            permission_id: p.id
          }
        end
      end.flatten
    )
  end

  # clears effective_permissions for itself and all
  # child objects
  def clear_effective_permissions
    EffectivePermission.where(
      geo_ref: self_and_descendents
    ).delete_all
  end

  # recursive assign effective permissions for
  # one permission object
  def assign_effective_permission(permission)
    EffectivePermission.insert_all(
      self_and_descendents.map do |g|
        {
          geo_ref_id: g.id,
          permission_id: permission.id
        }
      end
    )
  end

  # recursive assign effective permissions for
  # all permissions in this object tree
  def assign_effective_permissions
    Permission.where(
      geo_ref: self_and_descendents
    ).each do |p|
      p.geo_ref.assign_effective_permission(p)
    end

    return
  end
end
