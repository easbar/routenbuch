require 'routenbuch/version'
require 'routenbuch/features'
require 'routenbuch/sys_info'
require 'vips'

module Routenbuch
  def self.root
    Pathname.new(File.expand_path('..', __dir__))
  end

  def self.config
    Settings
  end

  def self.instance_name
    Settings[:instance_name] || 'Routenbuch'
  end

  def self.features
    @features ||= Routenbuch::Features.new(Settings.features)
  end

  def self.public?
    Settings[:public] == true
  end

  def self.sign_up?
    Settings[:sign_up] == true
  end

  def self.max_upload_size
    size = if Settings.key? :max_upload_size
             Settings[:max_upload_size].to_i
           else
             20
           end

    size.megabytes
  end

  LOG_LEVELS = %w[debug info warn error fatal unknown].freeze

  def self.log_level
    level = Settings[:log_level] || 'info'

    raise "log_level must be one of #{LOG_LEVELS.join(', ')}" \
      unless LOG_LEVELS.include? level

    return level.to_sym
  end

  LOG_FORMATS = %w[json key-value].freeze

  def self.log_format
    format = Settings[:log_format] || 'key-value'

    raise "log_format must be one of #{LOG_FORMATS.join(', ')}" \
      unless LOG_FORMATS.include? format

    return format.to_sym
  end

  def self.log_user
    enabled = if Settings.key? :log_user
                Settings[:log_user]
              else
                false
              end

    raise "log_user must be a true or false (boolean)" \
      unless [true, false].include? enabled

    return enabled
  end

  LOCALES = {
    de: 'Deutsch',
    en: 'English'
  }.freeze

  def self.selectable_locales
    LOCALES
  end

  def self.locales
    LOCALES.keys
  end

  def self.default_locale
    if Settings.key? :default_locale
      Settings[:default_locale].to_sym
    else
      :en
    end
  end

  def self.supported_image_types
    return @supported_image_types unless @supported_image_types.nil?

    @supported_image_types = []
    {
      png: %i[png],
      jpeg: %i[jpg jpeg],
      gif: %i[gif],
      heif: %i[avif],
    }.each do |loader, mime_types|
      if Vips::Image.respond_to? "#{loader}load"
        @supported_image_types += mime_types
      end
    end

    @supported_image_types
  end

  def self.monitoring_ip_whitelist
    whitelist = Settings[:monitoring_ip_whitelist] || %w[127.0.0.1]
    raise "monitoring_ip_whitelist must be an Array!" unless whitelist.is_a? Array

    whitelist
  end

  def self.sys_info
    @sys_info ||= SysInfo.new
  end

  def self.version
    Routenbuch::Version::VERSION
  end
end
