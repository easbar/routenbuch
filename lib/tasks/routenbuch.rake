namespace :routenbuch do
  desc "fix tags assigned to wrong model"
  task fix_tag_model: :environment do
    AssignedTag.all.each do |at|
      next if at.taggable.class.name == at.tag.model

      existing_tag = Tag.find_by(model: at.taggable.class.name, name: at.tag.name)
      at.tag = if existing_tag
                 existing_tag
               else
                 new_tag = at.tag.dub
                 new_tag.model = at.taggable.class.name
                 new_tag.save!
                 new_tag
               end
      at.save!
    end
  end

  desc "generade erd diagram with rails6 workarounds"
  task erd: :environment do
    say "Loading application environment..."
    Rake::Task[:environment].invoke

    say "Loading code in search of Active Record models..."
    Zeitwerk::Loader.eager_load_all

    Rake::Task['erd'].invoke
  end
end
