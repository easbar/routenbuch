module Routenbuch
  class SysInfo
    def rails_version
      @rails_version ||= Rails::VERSION::STRING
    end

    def ruby_version
      @ruby_version ||= "#{RUBY_VERSION}p#{RUBY_PATCHLEVEL}"
    end

    def postgres_version
      @postgres_version ||= ActiveRecord::Base.connection.select_value('SELECT version()')
    end

    def redis_version
      @redis_version ||= Sidekiq.redis_info['redis_version']
    end
    
    def vips_version
      @vips_version ||= Vips.version_string
    end
  end
end
