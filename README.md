# Routenbuch Applikation

Personal "climbing dairy" application.

## Development

The application is containerized.

### Start development instance

To startup a development instance `docker-compose` can be used:

```
docker-compose up
```

After startup the application can be access by browser:

* [Open browser on localhost:3000](http://localhost:3000)

### Initialize database

On first start the database must be initialized:

```
docker-compose exec web rake db:setup
```

### Create initial admin user

Use `rails console` to create an initial admin user:

```
docker-compose exec web rails console
```

In rails console create the user using the `User` model class:

```ruby
User.create!(email: 'root@localhost', password: 'secret', role: 'admin')
```

