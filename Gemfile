source 'https://rubygems.org'

gem 'puma'
gem 'rails', '~> 6.0.0'
gem 'pg'
gem "seedbank"

gem 'webpacker'
gem 'sassc-rails'

gem 'simple-navigation', '~> 4.0'
gem 'simple_navigation_bootstrap'
gem 'simple_form'

# Use ActiveModel has_secure_password
gem 'bcrypt'
gem 'strong_password', '~> 0.0.8'

# provide a health_check endpoint for monitoring
gem "health_check"

gem 'devise'
gem 'devise-i18n'
gem 'cancancan'
gem 'ransack'
gem 'pg_search'
gem 'kaminari'
gem 'redcarpet'

# uploads
gem 'image_processing'
gem 'active_storage_validations'

gem 'sidekiq'

# for geo coding
gem 'geokit-rails'

gem 'jsonapi-serializer'

gem 'gettext_i18n_rails'
gem 'settingslogic'
gem 'lograge'

group :development do
  gem 'gettext', '>=3.0.2', require: false
  gem 'web-console'
  gem 'byebug'
  gem 'spring'
  gem "better_errors"
  gem "binding_of_caller"
  gem 'listen'
  gem 'rails-erd'
  gem 'bundle-audit'
end

group :development, :test do
  gem 'rspec-rails', '~> 4.0.1'
  gem "factory_bot_rails"
  gem 'database_cleaner-active_record'
  gem 'simplecov', require: false
end
