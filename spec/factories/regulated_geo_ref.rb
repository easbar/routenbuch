FactoryBot.define do
  factory :regulated_geo_ref do
    regulation

    geo_ref factory: :crag
    reason { 'KZG' }

    after(:create) do |object, evaluator|
      create_list(:inspection, 3, regulated_geo_ref: object)
    end
  end
end
