require 'rails_helper'

RSpec.describe Inspection, type: :model do
  let(:attributes) { {} }
  let(:inspection) { create :inspection, **attributes }

  context 'inspection' do
    it 'validation' do
      expect { inspection.validate! }.not_to raise_error
    end

    it 'associations' do
      expect(inspection.regulated_geo_ref).to be_a(RegulatedGeoRef)
    end

    context 'without when' do
      let(:attributes) { { when: '' } }

      it 'validation' do
        expect { inspection.validate! }.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end
end
