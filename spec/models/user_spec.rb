require 'rails_helper'

RSpec.describe User, type: :model do
  let(:attributes) { {} }
  let(:user) { create :user, **attributes }

  context 'user' do
    it 'validation' do
      expect { user.validate! }.not_to raise_error
    end

    context 'with weak password' do
      let(:attributes) { { password: 'weak1234' } }

      it 'validation' do
        expect { user.validate! }.to raise_error /Password is too weak/
      end
    end
  end
end
