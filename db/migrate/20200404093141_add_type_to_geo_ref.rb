class AddTypeToGeoRef < ActiveRecord::Migration[6.0]
  def up
    add_column :geo_refs, :type, :string, null: false, default: 'Crag'
    add_index :geo_refs, :type

    GeoRef.all.includes(:geo_ref_type).each do |g|
      next unless g.geo_ref_type.present?

      g.type = g.geo_ref_type.name.camelize
      g.save!
    end
  end

  def down
    remove_index :geo_refs, :type
    remove_column :geo_refs, :type
  end
end
