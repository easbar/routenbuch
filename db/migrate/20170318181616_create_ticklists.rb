class CreateTicklists < ActiveRecord::Migration[5.0]
  def change
    create_table :ticklists do |t|
      t.references :user, foreign_key: true
      t.references :route, foreign_key: true
      t.string :comment

      t.timestamps
    end
    add_index :ticklists, [:user_id, :route_id], :unique => true
  end
end
