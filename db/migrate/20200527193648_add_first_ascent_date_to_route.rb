class AddFirstAscentDateToRoute < ActiveRecord::Migration[6.0]
  def change
    add_column :routes, :first_ascent_year, :integer, null: true
    add_column :routes, :first_ascent_month, :integer, null: true
    add_column :routes, :first_ascent_day, :integer, null: true
  end
end
