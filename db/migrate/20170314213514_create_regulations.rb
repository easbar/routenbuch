class CreateRegulations < ActiveRecord::Migration[5.0]
  def change
    create_table :regulations do |t|
      t.string :name
      t.text :body
      t.string :organization

      t.timestamps
    end
  end
end
