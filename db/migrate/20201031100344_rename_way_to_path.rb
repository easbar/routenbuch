class RenameWayToPath < ActiveRecord::Migration[6.0]
  def change
    rename_table :ways, :paths
    rename_table :geo_refs_ways, :geo_refs_paths
    rename_column :geo_refs_paths, :way_id, :path_id
  end
end
