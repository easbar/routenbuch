class AddBodyDescriptionToGeoRef < ActiveRecord::Migration[5.0]
  def change
    add_column :geo_refs, :description, :string
    add_column :geo_refs, :body, :text
  end
end
