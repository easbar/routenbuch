class CreateAscents < ActiveRecord::Migration[5.0]
  def change
    create_table :ascents do |t|
      t.references :user, foreign_key: true
      t.references :route, foreign_key: true
      t.text :comment
      t.date :when

      t.timestamps
    end
  end
end
