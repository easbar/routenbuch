class AddCategoryToTags < ActiveRecord::Migration[5.1]
  def change
    add_column :tags, :category, :string, null: false, default: 'other'
    add_column :tags, :priority, :integer, null: false, default: 10
  end
end
