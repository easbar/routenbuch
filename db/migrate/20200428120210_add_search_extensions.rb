class AddSearchExtensions < ActiveRecord::Migration[6.0]
  def change
    %w[pg_trgm fuzzystrmatch].each do |ext|
      enable_extension ext unless extension_enabled?(ext)
    end
  end
end
