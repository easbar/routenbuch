class ExtendStyles < ActiveRecord::Migration[6.0]
  def up
    add_column :styles, :belay_style, :string
    add_column :styles, :sportive_style, :string
    add_column :styles, :priority, :integer, null: false, default: 50

    update_existing_styles

    change_column :styles, :belay_style, :string, null: false
    change_column :styles, :sportive_style, :string, null: false
  end

  def down
    remove_column :styles, :belay_style
    remove_column :styles, :sportive_style
    remove_column :styles, :priority
  end

  def update_existing_styles
    Style.find_each do |style|
      style.update(
        case style.name
        when 'red point'
         {
           name: 'Redpoint',
           belay_style: 'lead',
           sportive_style: 'free',
           priority: 100,
         }
        when 'project'
         {
           name: 'Attempt to redpoint',
           belay_style: 'lead',
           sportive_style: 'tainted',
           priority: 90,
         }
        when 'top rope'
         {
           name: 'Toprope',
           belay_style: 'toprope',
           sportive_style: 'free',
           priority: 70,
         }
        when 'flash'
         {
           name: 'Flash - Redpoint on first attempt',
           belay_style: 'lead',
           sportive_style: 'free',
           priority: 80,
         }
        when 'onsight'
         {
           name: 'Onsight - Redpoint with no prior beta',
           belay_style: 'lead',
           sportive_style: 'free',
           priority: 80,
         }
        when 'free solo'
         {
           name: 'Free solo - Redpoint without protection',
           belay_style: 'freesolo',
           sportive_style: 'free',
           priority: 20,
         }
        else
         raise "dont known how to upgrade climbing style #{style.name}"
        end
      )
      style.save!
    end
  end
end
