class RefactorClimbingRegulations < ActiveRecord::Migration[6.0]
  def change
    rename_table :regulations, :zones
    rename_table :secondary_regulations, :secondary_zones
    rename_column :geo_refs, :regulation_id, :zone_id
    rename_column :secondary_zones, :regulation_id, :zone_id

    create_table :regulations do |t|
      t.belongs_to :geo_ref
      t.string :name, null: false
      t.string :abbr
      t.text :body
      t.timestamps
    end

    create_table :regulated_geo_refs do |t|
      t.belongs_to :regulation
      t.belongs_to :geo_ref
      t.string :reason
      t.timestamps
    end
    add_index :regulated_geo_refs, %i[regulation_id geo_ref_id], unique: true

    create_table :inspections do |t|
      t.belongs_to :regulated_geo_ref
      t.date :when
      t.text :body
      t.timestamps
    end

    add_column :zones, :icon_path, :string, null: false, default: ''
  end
end
