class CreateGeoRefs < ActiveRecord::Migration
  def change
    create_table :geo_refs do |t|
      t.string :name
      t.references :geo_ref_type, index: true
      t.references :parent, index: true

      t.timestamps null: false
    end
  end
end
