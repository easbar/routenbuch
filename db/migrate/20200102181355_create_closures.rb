class CreateClosures < ActiveRecord::Migration[6.0]
  def change
    create_table :closures do |t|
      t.string :description, null: false, default: ''
      t.integer :regular_start_month, null: true
      t.integer :regular_start_day_of_month, null: true
      t.integer :regular_end_month, null: true
      t.integer :regular_end_day_of_month, null: true
      t.string :kind, null: false, default: 'flexible'

      t.timestamps
    end
  end
end
