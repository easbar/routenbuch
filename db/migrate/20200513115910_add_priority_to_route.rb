class AddPriorityToRoute < ActiveRecord::Migration[6.0]
  def change
    add_column :routes, :priority, :integer, null: false, default: 0
    add_index :routes, :priority
  end
end
