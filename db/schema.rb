# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_12_01_200456) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "fuzzystrmatch"
  enable_extension "pg_trgm"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "ascents", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "route_id"
    t.text "comment"
    t.date "when"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "style_id"
    t.integer "attempts"
    t.integer "pitches"
    t.index ["route_id"], name: "index_ascents_on_route_id"
    t.index ["style_id"], name: "index_ascents_on_style_id"
    t.index ["user_id"], name: "index_ascents_on_user_id"
  end

  create_table "assigned_tags", id: false, force: :cascade do |t|
    t.string "taggable_type"
    t.bigint "taggable_id"
    t.bigint "tag_id"
    t.index ["tag_id"], name: "index_assigned_tags_on_tag_id"
    t.index ["taggable_type", "taggable_id"], name: "index_assigned_tags_on_taggable_type_and_taggable_id"
  end

  create_table "closures", force: :cascade do |t|
    t.string "description", default: "", null: false
    t.integer "regular_start_month"
    t.integer "regular_start_day_of_month"
    t.integer "regular_end_month"
    t.integer "regular_end_day_of_month"
    t.string "kind", default: "flexible", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string "target_type"
    t.integer "target_id"
    t.bigint "user_id"
    t.text "body", default: "", null: false
    t.string "kind", default: "comment", null: false
    t.boolean "private", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "pinned", default: false, null: false
    t.index ["kind"], name: "index_comments_on_kind"
    t.index ["pinned"], name: "index_comments_on_pinned"
    t.index ["private"], name: "index_comments_on_private"
    t.index ["target_type", "target_id"], name: "index_comments_on_target_type_and_target_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "effective_permissions", id: false, force: :cascade do |t|
    t.bigint "geo_ref_id"
    t.bigint "permission_id"
    t.index ["geo_ref_id", "permission_id"], name: "index_effective_permissions_on_geo_ref_id_and_permission_id", unique: true
    t.index ["geo_ref_id"], name: "index_effective_permissions_on_geo_ref_id"
    t.index ["permission_id"], name: "index_effective_permissions_on_permission_id"
  end

  create_table "first_ascent_people", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.boolean "dead", default: false, null: false
    t.string "alternative_names", default: [], null: false, array: true
  end

  create_table "geo_refs", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
    t.text "body"
    t.decimal "lat", precision: 10, scale: 6
    t.decimal "lng", precision: 10, scale: 6
    t.integer "zone_id"
    t.integer "height"
    t.integer "orientation"
    t.json "stats"
    t.bigint "closure_id"
    t.string "access", default: "internal", null: false
    t.string "type", default: "Crag", null: false
    t.integer "priority", default: 0, null: false
    t.string "alternative_names", default: [], null: false, array: true
    t.index ["access"], name: "index_geo_refs_on_access"
    t.index ["closure_id"], name: "index_geo_refs_on_closure_id"
    t.index ["lat", "lng"], name: "index_geo_refs_on_lat_and_lng"
    t.index ["parent_id"], name: "index_geo_refs_on_parent_id"
    t.index ["priority"], name: "index_geo_refs_on_priority"
    t.index ["type"], name: "index_geo_refs_on_type"
    t.index ["zone_id"], name: "index_geo_refs_on_zone_id"
  end

  create_table "geo_refs_paths", id: false, force: :cascade do |t|
    t.bigint "geo_ref_id"
    t.bigint "path_id"
    t.index ["geo_ref_id"], name: "index_geo_refs_paths_on_geo_ref_id"
    t.index ["path_id"], name: "index_geo_refs_paths_on_path_id"
  end

  create_table "grades", id: :serial, force: :cascade do |t|
    t.string "scale"
    t.string "scope"
    t.string "grade"
    t.integer "difficulty"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
  end

  create_table "inspections", force: :cascade do |t|
    t.bigint "regulated_geo_ref_id"
    t.date "when"
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["regulated_geo_ref_id"], name: "index_inspections_on_regulated_geo_ref_id"
  end

  create_table "paths", force: :cascade do |t|
    t.string "name", null: false
    t.string "type", default: "Approach", null: false
    t.string "body", default: "", null: false
    t.string "access", default: "private", null: false
    t.bigint "geo_ref_id"
    t.index ["geo_ref_id"], name: "index_paths_on_geo_ref_id"
    t.index ["type"], name: "index_paths_on_type"
  end

  create_table "permissions", force: :cascade do |t|
    t.bigint "geo_ref_id"
    t.bigint "user_id"
    t.string "level", default: "editor", null: false
    t.index ["geo_ref_id", "user_id"], name: "index_permissions_on_geo_ref_id_and_user_id", unique: true
    t.index ["geo_ref_id"], name: "index_permissions_on_geo_ref_id"
    t.index ["user_id"], name: "index_permissions_on_user_id"
  end

  create_table "photos", id: :serial, force: :cascade do |t|
    t.string "description"
    t.string "target_type"
    t.integer "target_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "private", default: false, null: false
    t.boolean "pinned", default: false, null: false
    t.boolean "cover_photo", default: false, null: false
    t.bigint "user_id", null: false
    t.index ["cover_photo"], name: "index_photos_on_cover_photo"
    t.index ["pinned"], name: "index_photos_on_pinned"
    t.index ["private"], name: "index_photos_on_private"
    t.index ["target_type", "target_id"], name: "index_photos_on_target_type_and_target_id"
    t.index ["user_id"], name: "index_photos_on_user_id"
  end

  create_table "regulated_geo_refs", force: :cascade do |t|
    t.bigint "regulation_id"
    t.bigint "geo_ref_id"
    t.string "reason"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["geo_ref_id"], name: "index_regulated_geo_refs_on_geo_ref_id"
    t.index ["regulation_id", "geo_ref_id"], name: "index_regulated_geo_refs_on_regulation_id_and_geo_ref_id", unique: true
    t.index ["regulation_id"], name: "index_regulated_geo_refs_on_regulation_id"
  end

  create_table "regulations", force: :cascade do |t|
    t.bigint "geo_ref_id"
    t.string "name", null: false
    t.string "abbr"
    t.text "body"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["geo_ref_id"], name: "index_regulations_on_geo_ref_id"
  end

  create_table "routes", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "body"
    t.integer "grade_id"
    t.integer "first_ascent_person_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "geo_ref_id"
    t.string "description"
    t.integer "pitches"
    t.integer "priority", default: 0, null: false
    t.integer "first_ascent_year"
    t.integer "first_ascent_month"
    t.integer "first_ascent_day"
    t.string "alternative_names", default: [], null: false, array: true
    t.index ["first_ascent_person_id"], name: "index_routes_on_first_ascent_person_id"
    t.index ["geo_ref_id"], name: "index_routes_on_geo_ref_id"
    t.index ["grade_id"], name: "index_routes_on_grade_id"
    t.index ["priority"], name: "index_routes_on_priority"
  end

  create_table "season_closures", force: :cascade do |t|
    t.bigint "closure_id", null: false
    t.string "description"
    t.integer "year", null: false
    t.integer "start_month"
    t.integer "start_day_of_month"
    t.integer "end_month"
    t.integer "end_day_of_month"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["closure_id", "year"], name: "index_season_closures_on_closure_id_and_year", unique: true
    t.index ["closure_id"], name: "index_season_closures_on_closure_id"
  end

  create_table "secondary_zones", force: :cascade do |t|
    t.string "description", default: "", null: false
    t.bigint "geo_ref_id", null: false
    t.bigint "zone_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["geo_ref_id"], name: "index_secondary_zones_on_geo_ref_id"
    t.index ["zone_id"], name: "index_secondary_zones_on_zone_id"
  end

  create_table "styles", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "redpoint", default: true
    t.string "belay_style", null: false
    t.string "sportive_style", null: false
    t.integer "priority", default: 50, null: false
    t.index ["name"], name: "index_styles_on_name", unique: true
  end

  create_table "tags", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category", default: "other", null: false
    t.integer "priority", default: 10, null: false
    t.string "model", default: "Crag", null: false
    t.string "icon"
    t.index ["model", "name"], name: "index_tags_on_model_and_name", unique: true
    t.index ["model"], name: "index_tags_on_model"
    t.index ["name"], name: "index_tags_on_name"
  end

  create_table "ticklists", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.integer "route_id"
    t.string "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["route_id"], name: "index_ticklists_on_route_id"
    t.index ["user_id", "route_id"], name: "index_ticklists_on_user_id_and_route_id", unique: true
    t.index ["user_id"], name: "index_ticklists_on_user_id"
  end

  create_table "topos", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "target_type"
    t.integer "target_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["target_type", "target_id"], name: "index_topos_on_target_type_and_target_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role", null: false
    t.string "name"
    t.text "bio"
    t.json "stats"
    t.string "access", default: "private", null: false
    t.string "ascents_access", default: "private", null: false
    t.string "ticklists_access", default: "private", null: false
    t.string "locale"
    t.index ["access"], name: "index_users_on_access"
    t.index ["ascents_access"], name: "index_users_on_ascents_access"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["ticklists_access"], name: "index_users_on_ticklists_access"
  end

  create_table "zones", id: :serial, force: :cascade do |t|
    t.string "name"
    t.text "body"
    t.string "organization"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "icon_path", default: "", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "ascents", "routes"
  add_foreign_key "ascents", "styles"
  add_foreign_key "ascents", "users"
  add_foreign_key "geo_refs", "closures"
  add_foreign_key "geo_refs", "zones"
  add_foreign_key "geo_refs_paths", "geo_refs"
  add_foreign_key "geo_refs_paths", "paths"
  add_foreign_key "paths", "geo_refs"
  add_foreign_key "photos", "users"
  add_foreign_key "regulated_geo_refs", "geo_refs"
  add_foreign_key "regulated_geo_refs", "regulations"
  add_foreign_key "regulations", "geo_refs"
  add_foreign_key "routes", "first_ascent_people"
  add_foreign_key "routes", "geo_refs"
  add_foreign_key "routes", "grades"
  add_foreign_key "season_closures", "closures"
  add_foreign_key "secondary_zones", "geo_refs"
  add_foreign_key "secondary_zones", "zones"
  add_foreign_key "ticklists", "routes"
  add_foreign_key "ticklists", "users"
end
